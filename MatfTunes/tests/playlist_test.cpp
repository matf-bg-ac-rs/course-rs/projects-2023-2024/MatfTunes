#include "catch.hpp"

#include "../src/playlist.h"
#include "../src/implicitplaylist.h"

TEST_CASE("Testing Playlist class", "[class]") {
    SECTION("Testing adding song") {
        //arange

        //action
        Playlist* p = new Playlist;
        Song* s = new Song("1.mp3");
        p->addSong(s);

        const auto a = p->songs();

        //assert
        REQUIRE(a[0] == s);
        delete p;
        delete s;
    }

    SECTION("Testing remove song") {
        Playlist* p = new Playlist;
        Song* s1 = new Song("1.mp3");
        Song* s2 = new Song("2.mp3");
        Song* s3 = new Song("3.mp3");
        p->addSong(s1);
        p->addSong(s2);
        p->addSong(s3);

        p->removeSong(s2);

        for (auto el : p->songs()) {
            REQUIRE_FALSE(el == s2);
        }

        delete p;
        delete s1;
        delete s2;
        delete s3;
    }

    SECTION("Testing playlist playback") {
        Playlist* p = new Playlist;
        Song* s1 = new Song("1.mp3");
        Song* s2 = new Song("2.mp3");
        Song* s3 = new Song("3.mp3");
        p->addSong(s1);
        p->addSong(s2);
        p->addSong(s3);

        REQUIRE(p->next() == s2);
        REQUIRE(p->previous() == s1);

        auto valid = p->shuffle();
        bool exists = false;
        for (auto el : p->songs()) {
            if (el == valid) {
                exists = true;
            }
        }
        REQUIRE(exists == true);
        delete p;
        delete s1;
        delete s2;
        delete s3;
    }

    SECTION("Testing playlist virtual methods changeName and changeCover") {
        Playlist* p = new Playlist;

        QString testName = "NewName";
        p->changeName(testName);
        REQUIRE(p->name() == testName);

        QString testCover = "default_artist_img.jpg";
        p->changeCover(testCover);
        REQUIRE(p->coverPath() == testCover);
        delete p;
    }
}

TEST_CASE("Testing ImplicitPlaylist class","[class]") {
    SECTION("Testing playlist virtual methods changeName and changeCover on ImplicitPlaylist class") {
        ImplicitPlaylist* liked = new ImplicitPlaylist;

        QString testName = "NewName";
        REQUIRE_THROWS(liked->changeName(testName));

        QString testCover = "default_artist_img.jpg";
        REQUIRE_THROWS(liked->changeCover(testCover));

        delete liked;
    }
}
