#ifndef EDITPLAYLISTUI_H
#define EDITPLAYLISTUI_H

#include <QMainWindow>
#include <QFileDialog>
#include <QPixmap>
#include "playlist.h"
#include "playlistitemui.h"
#include "playlistlibrary.h"

namespace Ui {
class EditPlaylistUI;
}

class EditPlaylistUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit EditPlaylistUI(Playlist* playlist, PlaylistItemUI* item, QMainWindow *parent = nullptr);
    ~EditPlaylistUI();

private slots:
    void save_edited_playlist();
    void change_icon();
signals:
    void saved_changes(PlaylistItemUI*);
private:
    Ui::EditPlaylistUI *ui;
    Playlist* m_playlist;
    PlaylistItemUI* m_playlist_item;
    QString newPathToImg = "";

    void setupUI();
};

#endif // EDITPLAYLISTUI_H
