#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "song.h"
#include "editsongui.h"
#include "songlibrary.h"
#include "songitemui.h"
#include "playlistlibrary.h"
#include "settings.h"
#include "playlist.h"
#include "playlistitemui.h"
#include "songlibrarywindow.h"
#include "playeritemui.h"
#include "editplaylistui.h"
#include "implicitplaylist.h"
#include "threadhandler.h"

#include <QPixmap>
#include <QFileDialog>
#include <QInputDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QIcon>
#include <QStandardPaths>
#include <QMutex>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    m_thread_handler = new ThreadHandler;
    connect(m_thread_handler,&ThreadHandler::songFound,this,&MainWindow::onThreadFinish);
    m_lastVisitedPage = 0;
    ui->setupUi(this);
    setWindowTitle("MatfTunes");
    reloadSongLibrary();
    reloadPlaylists();

    setupIcons();
    setupMainPage();
    setupSongLibPage();
    setupPlaylistPage();
    setupPlayer();

}


MainWindow::~MainWindow()
{
    auto w = ui->centralwidget->width();
    auto h = ui->centralwidget->height();
    Settings::settings().setWidth(w);
    Settings::settings().setHeight(h);
    delete ui;
    delete m_thread_handler;
}


// PRIVATE METHODS:

void MainWindow::setupIcons()
{
    ui->addSongButtonMP->setIcon(QIcon(":/resources/add_song_white_button.png"));
    ui->addPlaylistButtonMP->setIcon(QIcon(":/resources/add_playlist.png"));
    ui->settings_button->setIcon(QIcon(":resources/settings.png"));
    ui->back_button->setIcon(QIcon(":resources/go-back.png"));
    ui->btn_play_playlist->setIcon(QIcon(":/resources/play.png"));
    ui->btn_play_playlist->setIconSize(QSize(20, 20));
    ui->showLyricsButton->setIcon(QIcon(":/resources/microphone.png"));
    ui->showLyricsButton->setIconSize(QSize(40,40));
    ui->lyricText->setVisible(false);
    ui->search_songs_button->setIcon(QIcon(":/resources/search.png"));
    ui->searchPlaylistButton->setIcon(QIcon(":/resources/search.png"));
    ui->add_song_button_songs->setIcon(QIcon(":/resources/add_song_button.png"));
    ui->btn_add_song->setIcon(QIcon(":/resources/add_song_white_button.png"));
}

void MainWindow::setupPlayer()
{
    PlayerItemUI *player = new PlayerItemUI(this);
    m_playeritem = player;
    ui->invisible_player->addWidget(player);
    player->setVisible(false);
    connect(ui->showLyricsButton, &QPushButton::clicked, this,&MainWindow::showSongLyrics);
    connect(player, &PlayerItemUI::full_screen_signal, this, &MainWindow::fullScreenPlayer);
    connect(player, &PlayerItemUI::close_player_signal, this, &MainWindow::closePlayer);
    connect(player, &PlayerItemUI::songChanged, this, &MainWindow::updatePlayerSongInfo);
    connect(player, &PlayerItemUI::liked_song_signal, this, &MainWindow::receiveLike);
    connect(this, &MainWindow::deletedSong, player, &PlayerItemUI::songDeleted);
}

void MainWindow::setupMainPage()
{
    connect(ui->settings_button, &QToolButton::clicked, this, &MainWindow::settingsButton);
    connect(ui->back_button, &QPushButton::clicked, this, &MainWindow::backButton);
    connect(ui->songsButton, &QPushButton::clicked, this, &MainWindow::songsMainPageButton);
    connect(ui->addSongButtonMP, &QPushButton::clicked, this, &MainWindow::addSongButton);
    connect(ui->playlistsButton, &QPushButton::clicked, this, &MainWindow::playlistsMainPageButton);
    connect(ui->addPlaylistButtonMP, &QPushButton::clicked, this, &MainWindow::addPlaylistButton);
    connect(ui->topTracksSave, &QPushButton::clicked, this, &MainWindow::setupTopTracks);

    QPixmap logoPix(":/resources/matfTunes_LOGO_inv.png");
    ui->cover_img->setAlignment(Qt::AlignCenter);
    ui->logoLabel->setPixmap(logoPix.scaled(500, 500));

    setupTopTracks();
}

void MainWindow::setupSongLibPage()
{
    ui->songListWidget->setSelectionMode(QAbstractItemView::NoSelection);
    ui->searchSong->setVisible(false);
    ui->searchSong->setStyleSheet("background-color: #49314d");
    ui->leLyrics->setStyleSheet("background-color: #49314d");
    //ui->searchLyrics->setStyleSheet("background-color: #3f4564");
    connect(ui->add_song_button_songs, &QPushButton::clicked, this, &MainWindow::addSongButton);
    connect(ui->searchSong, &QLineEdit::textChanged, this, &MainWindow::filterSongItemUI);
    connect(ui->searchLyrics, &QPushButton::clicked, this, &MainWindow::searchLyrics);
    connect(ui->search_songs_button, &QPushButton::clicked, this, &MainWindow::searchSongsChangeUI);
}

void MainWindow::setupPlaylistPage()
{
    PlaylistLibrary::playlistLibrary();
    connect(this, &MainWindow::playlistDeleted, &PlaylistController::controller(), &PlaylistController::playlistDeleted);
    ui->playlists->setSelectionMode(QAbstractItemView::NoSelection);
    ui->playlistSongs->setSelectionMode(QAbstractItemView::NoSelection);
    ui->searchPlaylist->setVisible(false);
    ui->searchPlaylist->setStyleSheet("background-color: #49314d");
    connect(ui->btn_create_new_playlist, &QPushButton::clicked, this, &MainWindow::showCreateNewPlaylist);
    connect(ui->btn_save_new_playlist, &QPushButton::clicked, this, &MainWindow::saveNewPlaylist);
    ui->le_new_playlist->setVisible(false);
    ui->btn_save_new_playlist->setVisible(false);
    connect(ui->btn_add_song, &QPushButton::clicked, this, &MainWindow::openLibrary);
    connect(ui->btn_play_playlist, &QAbstractButton::clicked, this, &MainWindow::playPlaylist);
    connect(this, &MainWindow::refreshLiked, this, &MainWindow::refreshLikedShow);
    connect(ui->searchPlaylist, &QLineEdit::textChanged, this, &MainWindow::filterPlaylistItemUI);
    connect(ui->searchPlaylistButton, &QPushButton::clicked, this, &MainWindow::searchPlaylistsChangeUI);
    setCurrentPlaylist("Liked Songs");
    reloadCurrentPlaylist();
    showPlaylistCover(getCurrentPlaylist()->coverPath());

}

void MainWindow::clearListWidget(QListWidget* listWidget) {
    while (listWidget->count() > 0) {
        auto listWidgetItem = listWidget->takeItem(0);
        auto UIItem = listWidget->itemWidget(listWidgetItem); //TItemUI
        delete UIItem;
        delete listWidgetItem;
    }
}

void MainWindow::setupTopTracks()
{
    ui->topTracksListWidget->setSelectionMode(QAbstractItemView::NoSelection);
    clearListWidget(ui->topTracksListWidget);

    QVector<Song*> songsToShow;
    int numberOfTopTracks = (ui->topTracksEdit->text()).toInt();
    if(numberOfTopTracks == 0)
        songsToShow = SongLibrary::songLibrary().topNTracks(5);
    else
        songsToShow = SongLibrary::songLibrary().topNTracks(numberOfTopTracks);

    for(Song* s : songsToShow)
        displaySong(s, ui->topTracksListWidget);
}

void MainWindow::reloadSongLibrary() {
    clearListWidget(ui->songListWidget);
    QVector<Song*> songs = SongLibrary::songLibrary().songs();
    for(Song* s : songs){
        displaySong(s,ui->songListWidget);
    }
}


void MainWindow::reloadPlaylists()
{
    clearListWidget(ui->playlists);
    QVector<Playlist*> playlists = PlaylistLibrary::playlistLibrary().playlists();
    for (Playlist* p : playlists) {
        displayPlaylist(p);
    }
}




void MainWindow::saveCurrPageAndSetNext(Pages page) {
    int currPage = (int)ui->stacked_widget_pages->currentIndex();
    ui->stacked_widget_pages->setCurrentIndex((int)page);
    this->m_lastVisitedPage = currPage;
}

void MainWindow::showPlaylistCover(const QString path) {
    ui->btn_add_song->show();
    QPixmap coverImgPix(path);
    ui->cover_img->setAlignment(Qt::AlignCenter);
    ui->cover_img->setFixedSize(100, 100);
    ui->cover_img->setPixmap(coverImgPix.scaled(ui->cover_img->width(),ui->cover_img->width(),Qt::KeepAspectRatio));
}


void MainWindow::addRemoveLiked(Song* s) {
    ImplicitPlaylist* liked = PlaylistLibrary::playlistLibrary().likedSongs();
    if (s->liked()) {
        liked->addSong(s);
    } else {
        liked->removeSong(s);
    }
    emit refreshLiked();
}

Playlist* MainWindow::getCurrentPlaylist() const {
    if (ui->lbl_playlist_name->text() == "") {
        return nullptr;
    }
    return PlaylistLibrary::playlistLibrary().playlistByName(ui->lbl_playlist_name->text());
}

void MainWindow::setCurrentPlaylist(QString name) {
    ui->lbl_playlist_name->setText(name);
    showPlaylistCover(PlaylistLibrary::playlistLibrary().playlistByName(name)->coverPath());
}

void MainWindow::reloadCurrentPlaylist(){
    clearListWidget(ui->playlistSongs);
    Playlist *currentPlaylist = MainWindow::getCurrentPlaylist();

    if(currentPlaylist != nullptr){
        for(Song* s : currentPlaylist->songs())
            displaySong(s, ui->playlistSongs, currentPlaylist);
    }
}





// PRIVATE SLOTS:


// General

void MainWindow::backButton()
{
    saveCurrPageAndSetNext(Pages::MainPage);
}

void MainWindow::settingsButton()
{
    QString path = QFileDialog::getExistingDirectory(this,"Choose default directory", Settings::settings().defaultDir());
    if (path != "")
        Settings::settings().setDefaultDir(path.append("/"));
}


// Main page

void MainWindow::songsMainPageButton()
{
    saveCurrPageAndSetNext(Pages::SongLibPage);
}

void MainWindow::playlistsMainPageButton()
{
    saveCurrPageAndSetNext(Pages::PlaylistsPage);
}

void MainWindow::addSongButton()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFiles);
    auto paths = dialog.getOpenFileNames(this, "Add song", QStandardPaths::writableLocation(QStandardPaths::MusicLocation), tr("Audio files (*.mp3 *.wav)"));
    for (QString& path : paths) {
        if (path != "") {
            Song* song = SongLibrary::songLibrary().addSongViaPath(path);

            if (song == nullptr) {
                return;
            }

            displaySong(song,ui->songListWidget);
            saveCurrPageAndSetNext(Pages::SongLibPage);

            updateAllSongsPlaylist(song,true);
        } else {
            qDebug() << "No song selected!";
        }
    }
}

void MainWindow::addPlaylistButton()
{
    ui->stacked_widget_pages->setCurrentIndex((int)Pages::PlaylistsPage);
    showCreateNewPlaylist();
}




// Playlists page

void MainWindow::showCreateNewPlaylist()
{
    ui->le_new_playlist->setVisible(true);
    ui->le_new_playlist->clear();
    ui->btn_save_new_playlist->setVisible(true);
    ui->btn_create_new_playlist->setVisible(false);
}

void MainWindow::hideCreateNewPlaylist()
{
    ui->le_new_playlist->setVisible(false);
    ui->btn_save_new_playlist->setVisible(false);
    ui->btn_create_new_playlist->setEnabled(true);
    ui->btn_create_new_playlist->setVisible(true);
    connect(ui->btn_create_new_playlist, &QPushButton::clicked, this, &MainWindow::showCreateNewPlaylist);
}

void MainWindow::saveNewPlaylist()
{
    QString new_name = ui->le_new_playlist->text();

    displayPlaylist(PlaylistLibrary::playlistLibrary().addPlaylist(new_name));
    hideCreateNewPlaylist();

    saveCurrPageAndSetNext(Pages::PlaylistsPage);
}

//void MainWindow::showLikedSongs() {

//}

void MainWindow::removeSongFromPlaylist(Song* s)
{
    for (auto pl : PlaylistLibrary::playlistLibrary().playlists()) {
        pl->removeSong(s);
    }
}

void MainWindow::refreshLikedShow()
{
    auto currentPlaylist = getCurrentPlaylist();
    if (currentPlaylist == nullptr)
        return;
    if (currentPlaylist->name() == "Liked Songs") {
        auto liked = PlaylistLibrary::playlistLibrary().likedSongs();

        clearListWidget(ui->playlistSongs);

        for (Song* song : liked->songs()) {
            displaySong(song, ui->playlistSongs, liked);
        }
    }
}


void MainWindow::removePlaylist(PlaylistItemUI* pItem)
{
    if (dynamic_cast<ImplicitPlaylist*>(pItem->playlist())) {
        return;
    }

    Playlist* playlist = pItem->playlist();
    auto listItem = pItem->listItem();
    int row = ui->playlists->row(listItem);
    ui->playlists->takeItem(row);
    delete pItem;
    emit playlistDeleted(playlist);
    PlaylistLibrary::playlistLibrary().removePlaylist(playlist);

    setCurrentPlaylist("Liked Songs");
    ui->btn_add_song->hide();
    reloadCurrentPlaylist();

}


void MainWindow::showPlaylistSongs(PlaylistItemUI* item)
{
    clearListWidget(ui->playlistSongs);

    showPlaylistCover(item->playlist()->coverPath());
    ui->lbl_playlist_name->setText(item->playlist()->name());

    if (dynamic_cast<ImplicitPlaylist*>(getCurrentPlaylist())) { ui->btn_add_song->hide(); }
    auto playlist = item->playlist();
    QVector<Song*> songs = item->playlist()->songs();
    for (Song* song : songs) {
        displaySong(song, ui->playlistSongs, playlist);
    }
}

void MainWindow::openLibrary()
{
    auto currentPlaylist = MainWindow::getCurrentPlaylist();
    if (currentPlaylist != nullptr) {
        SongLibraryWindow *open_library = new SongLibraryWindow(currentPlaylist, this);
        connect(open_library, &SongLibraryWindow::songChosen, this, &MainWindow::receiveSong);
        connect(open_library, &SongLibraryWindow::songs_chosen, this, &MainWindow::receiveSongs);
        open_library->show();
    }
}


void MainWindow::playPlaylist()
{
    auto currentPlaylist = MainWindow::getCurrentPlaylist();
    if (currentPlaylist != nullptr) {
        PlaylistController::controller().setPlaylist(currentPlaylist);
        PlaylistController::controller().playPlaylist();
    }
}

void MainWindow::editPlaylist(PlaylistItemUI* playlist_item)
{
    EditPlaylistUI* playlist_window = new EditPlaylistUI(playlist_item->playlist(), playlist_item, this);
    playlist_window->setWindowModality(Qt::WindowModal);
    playlist_window->show();
    connect(playlist_window, &EditPlaylistUI::saved_changes, this, &MainWindow::updatePlaylistDetails);
}


void MainWindow::updatePlaylistDetails(PlaylistItemUI *p)
{
    auto name = p->playlist()->name();
    ui->lbl_playlist_name->setText(name);
    auto cover = p->playlist()->coverPath();
    showPlaylistCover(cover);
    showPlaylistSongs(p);
}

void MainWindow::receiveLike(Song *s)
{
    s->like();
    addRemoveLiked(s);
}

void MainWindow::receiveSong(Song *s, Playlist* p)
{
    displaySong(s, ui->playlistSongs, p);
}

void MainWindow::receiveSongs(QVector<Song*> songs, Playlist* p)
{
    for (auto s : songs)
    {
        displaySong(s, ui->playlistSongs, p);
    }
}






// SongItem

void MainWindow::removeSong(SongItemUI* songItem, QListWidget* list)
{
    Song* song = songItem->song();
    if (list == ui->playlistSongs) {
        auto listItem = songItem->listItem();
        int row = ui->songListWidget->row(listItem);
        ui->songListWidget->takeItem(row);
        delete songItem;
        delete listItem;
        emit deletedSong(song);
        PlaylistLibrary::playlistLibrary().playlistByName(ui->lbl_playlist_name->text())->removeSong(song);
    } else {

        auto listItem = songItem->listItem();
        int row = ui->songListWidget->row(listItem);
        ui->songListWidget->takeItem(row);
        delete songItem;
        delete listItem;
        emit deletedSong(song);
        SongLibrary::songLibrary().removeSong(song);
        updateAllSongsPlaylist(song,false);
        setupTopTracks();
    }
}


void MainWindow::editSong(SongItemUI* songItem)
{
    Song *song = songItem->song();
    EditSongUI* edit = new EditSongUI(song, songItem, this);

    connect(edit, &EditSongUI::savedSongChanges, songItem, &SongItemUI::updateSongDetails);
    connect(edit, &EditSongUI::savedSongChanges, m_playeritem, &PlayerItemUI::updateSongDetails);
    connect(edit, &EditSongUI::savedSongChanges, this, &MainWindow::setupTopTracks);
    connect(edit, &EditSongUI::savedSongChanges, this, &MainWindow::reloadCurrentPlaylist);
    connect(edit, &EditSongUI::savedSongChanges, this, &MainWindow::reloadSongLibrary);

    edit->show();
}




// Display


void MainWindow::displaySong(Song* s, QListWidget* list, Playlist* p)
{
    QListWidgetItem* item = new QListWidgetItem;
    SongItemUI* songItem = new SongItemUI(s, item, list, p, this);

    //TODO dogovoriti se da li TopTracks da bude implicit playlist
    if (list != ui->songListWidget && dynamic_cast<ImplicitPlaylist*>(getCurrentPlaylist()) || list == ui->topTracksListWidget) {
        songItem->hideForImplicit();
    }


    connect(songItem,&SongItemUI::deleteSongItem,this,&MainWindow::removeSong);
    connect(songItem,&SongItemUI::edit_song_clicked, this, &MainWindow::editSong);

    item->setSizeHint(songItem->sizeHint());
    list->addItem(item);
    list->setItemWidget(item,songItem);
}


void MainWindow::displayPlaylist(Playlist * p)
{
    QListWidgetItem *item = new QListWidgetItem;
    PlaylistItemUI* playlistItem = new PlaylistItemUI(p, item, this);

    connect(playlistItem,&PlaylistItemUI::clicked,this,&MainWindow::showPlaylistSongs);
    connect(this,&MainWindow::deletedSong,this, &MainWindow::removeSongFromPlaylist);
    connect(playlistItem, &PlaylistItemUI::deletePlaylistItem, this, &MainWindow::removePlaylist);
    connect(playlistItem, &PlaylistItemUI::edit_playlist_item, this, &MainWindow::editPlaylist);

    item->setSizeHint(QSize(ui->playlists->sizeHint().width(), 70));
    ui->playlists->addItem(item);
    ui->playlists->setItemWidget(item, playlistItem);
}




// Filter

void MainWindow::filterSongItemUI(QString token)
{
    QVector<Song*> songsToShow;
    if (token != "") {
        songsToShow = SongLibrary::songLibrary().songs(token);
    } else {
        songsToShow = SongLibrary::songLibrary().songs();
    }
    clearListWidget(ui->songListWidget);
    for (auto s : songsToShow) {
        displaySong(s, ui->songListWidget);
    }
}

void MainWindow::filterPlaylistItemUI(QString token)
{
    QVector<Playlist*> playlistsToShow;
    if (token != "") {
        playlistsToShow = PlaylistLibrary::playlistLibrary().playlists(token);
    } else {
        playlistsToShow = PlaylistLibrary::playlistLibrary().playlists();
    }

    clearListWidget(ui->playlists);

    for (auto p : playlistsToShow) {
        displayPlaylist(p);
    }
}







// Search

void MainWindow::searchSongsChangeUI()
{
    ui->searchSong->setVisible(not ui->searchSong->isVisible());
}

void MainWindow::searchPlaylistsChangeUI()
{
    ui->searchPlaylist->setVisible(not ui->searchPlaylist->isVisible());
}

void MainWindow::onThreadFinish(Song* s)
{
    displaySong(s,ui->songListWidget);
}

void MainWindow::searchLyrics()
{
    QString lyric = ui->leLyrics->text();
    clearListWidget(ui->songListWidget);
    m_thread_handler->startThreads(lyric);
}







// Player

void MainWindow::closePlayer()
{
    changePlayerVisible(false);
}

void MainWindow::fullScreenPlayer(bool full_screen)
{
    int currPage = (int)ui->stacked_widget_pages->currentIndex();
    if(full_screen){
        ui->settings_button->setVisible(true);
        ui->back_button->setVisible(true);
        ui->stacked_widget_pages->setCurrentIndex(this->m_lastVisitedPage);

    } else {
        ui->settings_button->setVisible(false);
        ui->back_button->setVisible(false);
        ui->stacked_widget_pages->setCurrentIndex((int)Pages::AudioPlayerPage);

    }
    this->m_lastVisitedPage = currPage;
}


void MainWindow::changePlayerVisible(bool visible)
{
    QLayoutItem* item;
    item = ui->invisible_player->itemAt(0);
    if (item != nullptr) {
        QWidget* widget = item->widget();
        widget->setVisible(visible);
        if (visible) {
            ui->player_frame->setStyleSheet("background-color: #000014; border: 1px solid #000014; border-radius: 15px;");
        }
        else {
            ui->player_frame->setStyleSheet("background-color: transparent; border: 0px;");
        }
    }
}

void MainWindow::updatePlayerSongInfo(Song* s)
{

    if (s != nullptr) {
        changePlayerVisible(true);
        ui->song_name_label->setText(s->name());
        ui->song_artist_label->setText(s->artist());
        ui->lyricText->setText(s->lyrics());
        QPixmap pix(s->pathToImg());
        ui->artist_img_label->setAlignment(Qt::AlignCenter);
        ui->artist_img_label->setFixedSize(500, 500);
        ui->artist_img_label->setPixmap(pix.scaled(ui->artist_img_label->width(),ui->artist_img_label->width(),Qt::KeepAspectRatio));
    } else {
        changePlayerVisible(false);
    }
}

void MainWindow::showSongLyrics()
{
    if (ui->lyricText->isVisible()) {
        ui->lyricText->setVisible(false);
    } else {
        ui->lyricText->setVisible(true);
    }
}

void MainWindow::updateAllSongsPlaylist(Song* song, bool add){
    if (add) {
        PlaylistLibrary::playlistLibrary().allSongs()->addSong(song);
    } else {
        PlaylistLibrary::playlistLibrary().allSongs()->removeSong(song);
    }
}


