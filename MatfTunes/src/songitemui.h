#ifndef SONGITEMUI_H
#define SONGITEMUI_H

#include <QWidget>
#include "song.h"
#include "playlist.h"
#include <QListWidgetItem>

namespace Ui {
class SongItemUI;
}

class SongItemUI : public QWidget
{
    Q_OBJECT

public:
    explicit SongItemUI(Song*, QListWidgetItem*, QListWidget*, Playlist*, QWidget *parent = nullptr);
    ~SongItemUI();

    inline QListWidgetItem* listItem() const { return m_list_item; }
    inline Song* song() const { return m_song; }
    void updateSongDetails();
    void hideForImplicit();
    void changeIcon(Song*);
public slots:
    void play_song();
    void remove_song();
    void edit_song();
signals:
    void song_played(SongItemUI*);
    void deleteSongItem(SongItemUI*, QListWidget*);
    void edit_song_clicked(SongItemUI*);
private:
    Ui::SongItemUI *ui;
    Song* m_song;
    QListWidgetItem* m_list_item;
    QListWidget* m_list;
    Playlist* m_playlist;
};

#endif // SONGITEMUI_H
