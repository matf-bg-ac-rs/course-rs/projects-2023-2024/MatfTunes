#ifndef LOGSLIDER_H
#define LOGSLIDER_H
#include <QSlider>
#include <QObject>

class LogSlider : public QSlider {
    Q_OBJECT
public:
  explicit LogSlider(QWidget* parent = nullptr, double scale = 1000);
  ~LogSlider();

  void setNaturalMin(double min);
  void setNaturalMax(double max);
  void setNaturalValue(double value);

  inline double scale() const { return m_scale; }

private slots:
  void onValueChanged(double value);

signals:
  void naturalValueChanged(double value);

private:
  double m_scale;
};

#endif // LOGSLIDER_H
