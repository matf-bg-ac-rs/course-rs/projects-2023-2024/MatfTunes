#ifndef PLAYLISTITEMUI_H
#define PLAYLISTITEMUI_H

#include <QWidget>
#include <QListWidgetItem>
#include <QPixmap>

#include "playlist.h"
namespace Ui {
class PlaylistItemUI;
}

class PlaylistItemUI : public QWidget
{
    Q_OBJECT

public:
    explicit PlaylistItemUI(Playlist* p, QListWidgetItem* item, QWidget *parent = nullptr);
    ~PlaylistItemUI();

    inline Playlist* playlist() const { return m_playlist; }
    void removePlaylist();
    inline QListWidgetItem* listItem() const { return m_item; }
    void edit_playlist();
    void edit_playlist_name();
    void set_playlist_icon(QString&);
    void hideForImplicit();

signals:
    void clicked(PlaylistItemUI*);
    void deletePlaylistItem(PlaylistItemUI*);
    void edit_playlist_item(PlaylistItemUI*);

protected:
    void mousePressEvent(QMouseEvent* event) override
    {
        emit clicked(this);
    }

private:
    Ui::PlaylistItemUI *ui;
    Playlist* m_playlist;
    QListWidgetItem* m_item;
    QPixmap* m_icon;
    QString m_icon_path = ":/resources/playlist.png";
};

#endif // PLAYLISTITEMUI_H
