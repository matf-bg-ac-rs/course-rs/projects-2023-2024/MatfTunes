#include "threadhandler.h"

ThreadHandler::ThreadHandler()
{
}

void ThreadHandler::startThreads(QString lyric) {
    QVector<LyricsSearchThread*> niti;
    for (auto song : SongLibrary::songLibrary().songs()) {
        LyricsSearchThread* nit = new LyricsSearchThread(song, lyric);
        niti.append(nit);
        connect(nit, &LyricsSearchThread::finishedSearch, this,&ThreadHandler::onThreadFinish);
        connect(nit, &LyricsSearchThread::finished, nit, &LyricsSearchThread::deleteLater);
        nit->start();
    }
    for (auto nit : niti)
        nit->wait();
}

void ThreadHandler::onThreadFinish(Song* s) {
    if (s == nullptr)
        return;
    emit songFound(s);
}
