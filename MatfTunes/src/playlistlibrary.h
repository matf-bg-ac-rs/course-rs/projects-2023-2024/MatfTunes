#ifndef PLAYLISTLIBRARY_H
#define PLAYLISTLIBRARY_H

#include "playlist.h"
#include "implicitplaylist.h"

class PlaylistLibrary : public Serializable
{
public:
    static PlaylistLibrary& playlistLibrary();

    void addPlaylist(Playlist*);
    Playlist* addPlaylist(QString &name);
    void removePlaylist(Playlist*);

    inline int numberOfPlaylists() const { return m_playlists.length(); }
    inline QVector<Playlist*> playlists() const { return m_playlists; }
    QVector<Playlist*> playlists(QString token);
    Playlist* playlistByName(QString name);
    inline ImplicitPlaylist* allSongs() const { return dynamic_cast<ImplicitPlaylist*>(m_playlists[0]); }
    inline ImplicitPlaylist* likedSongs() const { return dynamic_cast<ImplicitPlaylist*>(m_playlists[1]); }

    //Serializable
    QVariant toVariant() final;
    void fromVariant(const QVariant &variant);

    void savePlaylists();
    void loadPlaylists();

private:
    PlaylistLibrary();
    ~PlaylistLibrary();
    PlaylistLibrary(const PlaylistLibrary&) = delete;
    PlaylistLibrary& operator=(const PlaylistLibrary&) = delete;
    QVector<Playlist*> m_playlists;
 };

#endif // PLAYLISTLIBRARY_H
